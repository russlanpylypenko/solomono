<?php


namespace App\Controller;


use App\Models\Category;
use App\Models\Product;

class HomeController
{
    private function header()
    {
       require_once ROOT.'/views/header.php';
    }
    private function footer()
    {
        require_once ROOT.'/views/footer.php';
    }

    public function run()
    {
        $Categories = new Category();
        $categories = $Categories->getAll();

        $Products = new Product();
        $category_id = $categories[0]['id'];
        $order_by = 'id';

        if($_POST['product'] && $this->isAjaxRequest()){
            $Products->save($_POST['product']);
        }

        if(isset($_GET['category_id'])){
            $category_id = (int)$_GET['category_id'];
        }

        if(isset($_GET['order_by'])){
            $order_by = $_GET['order_by'];
        }

        switch ($order_by){
            case "name" :
                $order_by = "name";
                break;
            case "price":
                $order_by = "price";
                break;
            case "date_desc":
                $order_by = "created_at DESC";
                break;
        }

        $products = $Products->getByCategoryId($category_id, $order_by);


        if(isset($_GET['category_id']) && $this->isAjaxRequest()){
            echo json_encode(['products'=> $products]);
            return true;
        }

        if(isset($_GET['product_id']) && $this->isAjaxRequest()){
            $products = $Products->getProductById($_GET['product_id']);
            echo json_encode(['product'=> $products]);
            return true;
        }

        return $this->render("index", [
            'categories' => $categories,
            'active_category_id' => $category_id,
            'order_by' => isset($_GET['order_by']) ? $_GET['order_by'] : false,
            'products' => $products,
        ]);
    }

    private function render($page, $data){
        extract($data, EXTR_PREFIX_SAME, "wddx");
        $this->header();
        require_once ROOT.'/views/'.$page.".php";
        $this->footer();
        return true;
    }

    private function isAjaxRequest()
    {
        if(
            isset($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            !empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
            strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest')
        {
            return true;
        }
       return false;
    }

}