<?php


namespace App\Models;

use PDO;
class Category extends Model
{
    public $table = "categories";

    public function getAll(){
        $sql = "SELECT * FROM ".$this->table;
        $sth = $this->db->prepare($sql);
        $sth->execute();
        $data = $sth->fetchAll(PDO::FETCH_ASSOC);
        return $data;
    }
}