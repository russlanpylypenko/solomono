<?php


namespace App\Models;

use \PDO;

class Model
{
    private $host = 'localhost';
    private $db_name = 'solomono';
    private $user = "root";
    private $pass = "";
    public $db;

    public function __construct()
    {
        $this->db = new PDO("mysql:host=$this->host;dbname=$this->db_name", $this->user, $this->pass);
    }
}