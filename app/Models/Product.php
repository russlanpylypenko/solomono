<?php


namespace App\Models;

use PDO;

class Product extends Model
{

    public $table = "products";

    public function getByCategoryId($category_id, $order_by)
    {
        $sql = "SELECT * FROM  $this->table  WHERE `category_id` = :id ORDER BY ".$order_by;
        $sth = $this->db->prepare($sql);
        $sth->bindParam(':id', $category_id, PDO::PARAM_INT);
        $sth->execute();
        return $sth->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getProductById($id)
    {
        $sql = "SELECT * FROM  $this->table  WHERE `id` = :id";
        $sth = $this->db->prepare($sql);
        $sth->bindParam(':id', $id, PDO::PARAM_INT);
        $sth->execute();
        return $sth->fetch(PDO::FETCH_ASSOC);
    }

    public function save($product){
        $stmt = $this->db->prepare("INSERT INTO $this->table (name, price, category_id) VALUES (:name, :price, :category_id)");
        $stmt->bindParam(':name', $product['name']);
        $stmt->bindParam(':price', $product['price']);
        $stmt->bindParam(':category_id', $product['category_id']);
        $stmt->execute();
    }
}