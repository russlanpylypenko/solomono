<?php
require_once __DIR__ . '/vendor/autoload.php';

define('ROOT', __DIR__);

use App\Controller\HomeController;

$page = new HomeController();

$page->run();