$(document).ready(function () {
    var location;
    $(".category_list a").click(function (event) {
        event.preventDefault();
        $(".category_list a").removeClass('active');
        $(this).addClass('active');
        var category_id = $(this).data('categoryId');
        var order_by = 'id';
        if(getUrlVars()['order_by'] !== undefined){
            order_by = getUrlVars()['order_by'];
        }
        getProducts(category_id, order_by);
    });

    $("#order").change(function () {
        var order_by = $(this).val();
        var category_id = getUrlVars()['category_id'];
        if(category_id === undefined) category_id = 1;
        getProducts(category_id, order_by);
    });

    $(".buy_product_btn").click(function () {
        var product_id = $(this).data('productId');
        getProductById(product_id);
    });

    $("#submit_form_button").click(function () {
        var form = $("#add_product_window");
        var product = {};
        product.category_id = form.find("#category_id").val();
        product.name = form.find("#name").val();
        product.price = form.find("#price").val();
        var formValid = true;
        $('input').each(function () {
            var formGroup = $(this).parents('.form-group');
            if (this.checkValidity()) {
                formGroup.addClass('has-success').removeClass('has-error');
            } else {
                formGroup.addClass('has-error').removeClass('has-success');
                formValid = false;
            }
        });

        if(validateNumberField(product.category_id) == 0){
            form.find("#category_id").addClass('has-success').removeClass('has-error');
        }else{
            form.find("#category_id").addClass('has-error').removeClass('has-success');
            formValid = false;
        }
        if (formValid) {
            form.submit(function () {
                saveProduct(product);
                product = {};
                return false;
            });

        }
    });


    function validateNumberField(field) {
        var regexp = /^\d+$/;
        return field.search(regexp);
    }

    function saveProduct(product) {
        if (product !== undefined) {
            $.ajax({
                method: "POST",
                url: "/",
                data: {product: product}
            }).done(function (response) {
                $('#add_product_window').modal('hide');
                getProducts(getUrlVars()['category_id'], getUrlVars()['order_by']);
            });
        }
    }

    function getProductById(product_id) {
        if (product_id !== undefined) {
            $.ajax({
                method: "GET",
                url: "/",
                data: {product_id: product_id}
            }).done(function (response) {
                var data = eval("(" + response + ")");
                var product = data.product;
                renderProductModal(product);
            });
        }
    }


    function getProducts(category_id, order_by) {
        $.ajax({
            method: "GET",
            url: "/",
            data: {category_id: category_id, order_by: order_by}
        }).done(function (response) {
            location = "/?category_id=" + category_id;
            if (order_by !== undefined) {
                location += "&order_by=" + order_by;
            }
            window.history.pushState('', '', location);
            var data = eval("(" + response + ")");
            var products = data.products;
            renderProducts(products);
        });
    }


    function renderProducts(products) {
        var _this = $('#product-wrapper');
        _this.find('tr').remove();
        var html = null;
        for (var key in products) {
            html +=
                '<tr>' +
                '<td>' + products[key].name + '</td>' +
                '<td>' + products[key].price + '</td>' +
                '<td>' + products[key].created_at + '</td>' +
                '<td><button class="btn btn-info buy_product_btn" data-product-id="'+ products[key].id +'" data-toggle="modal" data-target="#product_window">Buy</button></td>'+
                '</tr>';
        }
        _this.html(html);
    }

    function renderProductModal(product) {
        var _this = $('#product_window');
        _this.find('.product_id').text("#" + product.id);
        _this.find('.product_name').text(product.name);
        _this.find('.product_price').text(product.price + " грн");
    }

    function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
            vars[key] = value;
        });
        return vars;
    }


});
