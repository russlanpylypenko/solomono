<div class="container">
    <div class="col-sm-4">
        <div class="list-group category_list">
            <?php foreach ($categories as $category): ?>
                <?php $class = ($active_category_id == $category['id']) ? 'active' : false; ?>
                <a href="#" data-category-id="<?= $category['id'] ?>" class="list-group-item <?= $class;?>"><?= $category['name'] ?></a>
            <?php endforeach; ?>
        </div>
        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#add_product_window">add product</button>
    </div>
    <div class="col-sm-8">
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Name</th>
                <th>Price</th>
                <th>Data</th>
                <th>Buy</th>
            </tr>
            </thead>
            <tbody id="product-wrapper">
            <form class="form-inline" id="form">
                <div class="form-group">
                    <label for="order">Ordered:</label>
                    <select class="form-control" id="order" name="order">
                        <option value="id" disabled <?php if(!$order_by or $order_by == 'id') echo "selected"?>>Сортировать по</option>
                        <option value="price" <?php if($order_by === 'price') echo "selected"?>>Від дешевших</option>
                        <option value="name" <?php if($order_by === 'name') echo "selected"?>>По алфавіту</option>
                        <option value="date_desc" <?php if($order_by === 'date_desc') echo "selected"?>>Нові</option>
                    </select>
                </div>
            </form>
                <?php foreach ($products as $product): ?>
                    <tr>
                        <td><?= $product['name']; ?></td>
                        <td><?= $product['price']; ?></td>
                        <td><?= $product['created_at']; ?></td>
                        <td><button class="btn btn-info buy_product_btn" data-product-id="<?= $product['id'] ?>" data-toggle="modal" data-target="#product_window">Buy</button></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>

<!-- Product modal start -->
<div id="product_window" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4>Товар номер № <span class="product_id">#00</span></h4>
            </div>
            <div class="modal-body">
                <p>Назва товару: <span class="product_name">Product name</span></p>
                <p>Цена: <span class="product_price">Product price</span></p>
            </div>
        </div>
    </div>
</div>
<!-- Product modal end -->

<!-- add product modal start -->
<div id="add_product_window" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4>Добавить товар</h4>
            </div>
            <div class="modal-body">
                <form id="add_product_form" method="post" role="form" data-toggle="validator">
                    <div class="form-group">
                        <label for="category_id">catrgory_id:</label>
                        <select name="category_id" class="form-control"  required id="category_id">
                            <?php foreach ($categories as $category): ?>
                                <option value="<?=$category['id'];?>" ><?= $category['name']?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="name">name:</label>
                        <input type="text" name="name" pattern=".{3,250}" required class="form-control" id="name">
                    </div>
                    <div class="form-group">
                        <label for="price">price:</label>
                        <input type="text" name="price" pattern="^\d+$" required class="form-control" id="price">
                    </div>
                    <button type="submit" id="submit_form_button" class="btn btn-default">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- add product modal end -->